<?php

class MY_Controller_OLD extends CI_Controller {
	
	// Data to pass into views
	protected 	$data = array();
	protected 	$mBodyClass = 'hold-transition skin-blue sidebar-mini fixed';
	public 		$mSettings = NULL;
	protected 	$mPageTitle = '';
	public 		$mActiveID = 'NULL';
	protected 	$mCtrler = 'home';		// current controller
	protected 	$mAction = 'index';		// controller function being called
	protected 	$mMenu = array();
	protected	$mUri = '';
	protected	$mConfig = array();

	// Login user
	protected 	$mPageAuth = array();
	protected 	$mUser = NULL;
	protected 	$mUserGroups = array();
	protected 	$mUserMainGroup;
	public 	  	$DB1;
	
	// Constructor
	public function __construct()
	{
		parent::__construct();
		$this->_setup();

	}

	protected function _setup(){
		$this->lang->load('main_lang');
		$this->load->model('settings_model');
		$this->mCtrler = $this->router->fetch_class();
		$this->mAction = $this->router->fetch_method();

		$this->mSettings = $this->settings_model->getSettings();
		$this->data['settings'] = $this->mSettings;
		
		$this->mConfig['login_url'] = base_url().'login';
		$this->mConfig['admin_url'] = base_url().'admin';
		$this->mConfig['home_url'] = base_url().'dashboard';

		if ( $this->ion_auth->logged_in() )
		{
			// $this->mUser = $this->ion_auth->user()->row();
			// $this->session->set_userdata('user', $this->mUser);
			// $this->data['perms'] = $this->settings_model->getGroupPermissions($this->ion_auth->get_users_groups()->row()->id);

			// foreach ($this->data['perms'] as $perm => $value) {
			// 	if (strpos($perm, '-') !== false) {
			// 		$perm = str_replace("-", "/", $perm);
			// 		$this->mPageAuth[$perm] = $value;
			// 	}
			// }
		}

		// restrict pages
		$this->mUri = $this->mCtrler.'/'.$this->mAction;
		
		// if ( array_key_exists($this->mUri, $this->mPageAuth) && !$this->mPageAuth[$this->mUri] == 1 )
		// {
		// 	$this->session->set_flashdata('warning', 'You dont have permissions to access this page');
		// 	redirect('dashboard');
		// }

		$this->mMenu = array();

		if ($this->ion_auth->is_admin()) {
				$this->mMenu['label1'] = array(
					'url'		=> '',
					'name'		=> lang('sidebar_menu_label1'),
				);
				$this->mMenu['admin-home']  = array(
					'name'		=> lang('sidebar_menu_admin_home'),
					'url'		=> 'admin/index',
					'icon'		=> 'fa fa-home',
				);
				$this->mMenu['admin-accounts']  = array(
					'name'		=> lang('sidebar_menu_admin_accounts'),
					'url'		=> 'admin',
					'icon'		=> 'fa fa-address-book',
					'children'  => array(
						lang('sidebar_menu_admin_accounts_child_manage')	=> 'accounts',
						lang('sidebar_menu_admin_accounts_child_create')	=> 'create_account',
					)
				);

				$this->mMenu['admin-users']  = array(
					'name'		=> lang('sidebar_menu_admin_users'),
					'url'		=> 'admin',
					'icon'		=> 'fa fa-users',
					'children'  => array(
						lang('sidebar_menu_admin_users_child_manage')	=> 'users',
						lang('sidebar_menu_admin_users_child_create')	=> 'create_user',
						lang('sidebar_menu_admin_users_child_groups')	=> 'getGroups',
					)
				);
				$this->mMenu['admin-settings']  = array(
					'name'		=> lang('sidebar_menu_admin_settings'),
					'url'		=> 'admin/settings',
					'icon'		=> 'fa fa-cogs',
				);
		}
		$this->mMenu['label'] = array(
			'url'		=> '',
			'name'		=> lang('sidebar_menu_label'),
		);
		$this->mMenu['home'] = array(
			'name'		=> lang('sidebar_menu_home'),
			'url'		=> 'dashboard/index',
			'icon'		=> 'fa fa-home',
		);
		$this->mMenu['accounts'] = array(
			'name'		=> lang('sidebar_menu_accounts'),
			'url'		=> 'accounts/index',
			'icon'		=> 'fa fa-sitemap',
		);
		$this->mMenu['entries'] = array(
			'name'		=> lang('sidebar_menu_entries'),
			'url'		=> 'entries/index',
			'icon'		=> 'fa fa-plus-square-o',
		);
		$this->mMenu['search'] = array(
			'name'		=> lang('sidebar_menu_search'),
			'url'		=> 'search/index',
			'icon'		=> 'fa fa-search',
		);
		$this->mMenu['reports'] = array(
			'name'		=> lang('sidebar_menu_reports'),
			'url'		=> 'reports',
			'icon'		=> 'fa fa-bar-chart',
			'children'  => array(
				lang('sidebar_menu_reports_child_balancesheet')		=> 'balancesheet',
				lang('sidebar_menu_reports_child_profitloss')		=> 'profitloss',
				lang('sidebar_menu_reports_child_trialbalance')		=> 'trialbalance',
				lang('sidebar_menu_reports_child_ledgerstatement')	=> 'ledgerstatement',
				lang('sidebar_menu_reports_child_ledgerentries')	=> 'ledgerentries',
				lang('sidebar_menu_reports_child_reconciliation')	=> 'reconciliation',
			)
		);

		$this->mMenu['account_settings'] = array(
			'name'		=> lang('sidebar_menu_account_settings'),
			'url'		=> 'account_settings',
			'icon'		=> 'fa fa-cog',
			'children'  => array(
				lang('sidebar_menu_account_settings_child_main')		=> 'main',
				lang('sidebar_menu_account_settings_child_cf')			=> 'cf',
				lang('sidebar_menu_account_settings_child_email')		=> 'email',
				lang('sidebar_menu_account_settings_child_printer')		=> 'printer',
				lang('sidebar_menu_account_settings_child_entrytypes')	=> 'entrytypes',
				lang('sidebar_menu_account_settings_child_tags')		=> 'tags',
				lang('sidebar_menu_account_settings_child_lock')		=> 'lock',
			)
		);

		// echo "<pre>";

		// print_r($this->ion_auth->get_users_groups()->row());
		// die();

		// echo "</pre>";


	}

	// set page title automatically
	public function setPageTitle()
	{
		switch(true)
	    {
	      	case ($this->mCtrler == 'admin' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_admin_index');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'log'):
		        // $this->mPageTitle = lang('page_title_admin_view_full_log');
		        break;
		    case ($this->mCtrler == 'admin' and $this->mAction == 'settings'):
		        // $this->mPageTitle = lang('page_title_admin_settings');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'users'):
		        // $this->mPageTitle = lang('page_title_admin_manage_users');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'create_user'):
		        // $this->mPageTitle = lang('page_title_admin_create_user');
		        break;
		    case ($this->mCtrler == 'admin' and $this->mAction == 'edit_user'):
		        // $this->mPageTitle = lang('page_title_admin_edit_user');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'accounts'):
		        // $this->mPageTitle = lang('page_title_admin_manage_accounts');
		        break;
		    case ($this->mCtrler == 'admin' and $this->mAction == 'create_account'):
		        // $this->mPageTitle = lang('page_title_admin_create_account');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'edit_permission'):
		        // $this->mPageTitle = lang('page_title_admin_edit_group_permission');
		        break;
			case ($this->mCtrler == 'admin' and $this->mAction == 'getGroups'):
		        // $this->mPageTitle = lang('page_title_admin_manage_user_groups');
		        break;
	    	case ($this->mCtrler == 'admin' and $this->mAction == 'create_group'):
		        // $this->mPageTitle = lang('page_title_admin_create_user_group');
		        break;
		    case ($this->mCtrler == 'admin' and $this->mAction == 'edit_group'):
		        // $this->mPageTitle = lang('page_title_admin_edit_user_group');
		        break;
	    	case ($this->mCtrler == 'user' and $this->mAction == 'activate'):
		        // $this->mPageTitle = lang('page_title_user_activate');
		        break;
		    case ($this->mCtrler == 'dashboard' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_dashboard_index');
		        break;
	    	case ($this->mCtrler == 'entries' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_entries_index');
		        break;
			case ($this->mCtrler == 'entries' and $this->mAction == 'add'):
		        // $this->mPageTitle = lang('page_title_entries_add');
		        break;
	    	case ($this->mCtrler == 'entries' and $this->mAction == 'edit'):
		        // $this->mPageTitle = lang('page_title_entries_edit');
		        break;
		    case ($this->mCtrler == 'entries' and $this->mAction == 'view'):
		        // $this->mPageTitle = lang('page_title_entries_view');
		        break;
		    case ($this->mCtrler == 'groups' and $this->mAction == 'add'):
		        // $this->mPageTitle = lang('page_title_groups_add');
		        break;
	    	case ($this->mCtrler == 'groups' and $this->mAction == 'edit'):
		        // $this->mPageTitle = lang('page_title_groups_edit');
		        break;
			case ($this->mCtrler == 'ledgers' and $this->mAction == 'add'):
		        // $this->mPageTitle = lang('page_title_ledgers_add');
		        break;
	    	case ($this->mCtrler == 'ledgers' and $this->mAction == 'edit'):
		        // $this->mPageTitle = lang('page_title_ledgers_edit');
		        break;
		    case ($this->mCtrler == 'login' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_login_index');
		        break;
		    case ($this->mCtrler == 'reports' and $this->mAction == 'balancesheet'):
		        // $this->mPageTitle = lang('page_title_reports_balancesheet');
		        break;
	    	case ($this->mCtrler == 'reports' and $this->mAction == 'profitloss'):
		        // $this->mPageTitle = lang('page_title_reports_profitloss');
		        break;
			case ($this->mCtrler == 'reports' and $this->mAction == 'trialbalance'):
		        // $this->mPageTitle = lang('page_title_reports_trialbalance');
		        break;
	    	case ($this->mCtrler == 'reports' and $this->mAction == 'ledgerstatement'):
		        // $this->mPageTitle = lang('page_title_reports_ledgerstatement');
		        break;
		    case ($this->mCtrler == 'reports' and $this->mAction == 'ledgerentries'):
		        // $this->mPageTitle = lang('page_title_reports_ledgerentries');
		        break;
	    	case ($this->mCtrler == 'reports' and $this->mAction == 'reconciliation'):
		        // $this->mPageTitle = lang('page_title_reports_reconciliation');
		        break;
		    case ($this->mCtrler == 'search' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_search_index');
		        break;
	    	case ($this->mCtrler == 'account_settings' and $this->mAction == 'cf'):
		        // $this->mPageTitle = lang('page_title_account_settings_cf');
		        break;
			case ($this->mCtrler == 'account_settings' and $this->mAction == 'main'):
		        // $this->mPageTitle = lang('page_title_account_settings_main');
		        break;
	    	case ($this->mCtrler == 'account_settings' and $this->mAction == 'entrytypes'):
		        // $this->mPageTitle = lang('page_title_account_settings_entrytype');
		        break;
		    case ($this->mCtrler == 'account_settings' and $this->mAction == 'lock'):
		        // $this->mPageTitle = lang('page_title_account_settings_lock');
		        break;
			case ($this->mCtrler == 'account_settings' and $this->mAction == 'tags'):
		        // $this->mPageTitle = lang('page_title_account_settings_tags');
		        break;
	    	case ($this->mCtrler == 'account_settings' and $this->mAction == 'printer'):
		        // $this->mPageTitle = lang('page_title_account_settings_printer');
		        break;
		    case ($this->mCtrler == 'account_settings' and $this->mAction == 'email'):
		        // $this->mPageTitle = lang('page_title_account_settings_email');
		        break;
		    case ($this->mCtrler == 'accounts' and $this->mAction == 'index'):
		        // $this->mPageTitle = lang('page_title_accounts_index');
		        break;
		    default:
		    	break;
	    }
	}

	// [Only Verify user login] (OR) [authenticate user group (and) Verify user login]
	protected function verify_login($redirect_url = NULL, $authenticate = FALSE)
	{
		if ( $redirect_url == NULL ){
			//if $redirect_url = NULL then store $this->mConfig['login_url']
			$redirect_url = $this->mUri;
		}

		if (!$this->ion_auth->logged_in()){
			//if not logged in redirect to login_url
			$this->session->set_flashdata('warning', 'Please Login to Continue');
			redirect($this->mConfig['login_url']);
		}else{
			$this->mUser = $this->ion_auth->user()->row();
			$this->session->set_userdata('user', $this->mUser);
			//if user logged in already add "login_verification" session data
			$this->session->set_userdata('login_verification', 'TRUE');
			$this->data['perms'] = $this->settings_model->getGroupPermissions($this->ion_auth->get_users_groups()->row()->id);
			$this->data['user_group'] = $this->ion_auth->get_users_groups()->row();

			foreach ($this->data['perms'] as $perm => $value) {
				if (strpos($perm, '-') !== false) {
					$perm = str_replace("-", "/", $perm);
					$this->mPageAuth[$perm] = $value;
				}
			}

			// echo "<pre>";
			// print_r($this->mPageAuth);
			// die();

			// authenticate user by group(s)
			if ($authenticate == true) {
				$key = NULL;
				$message = NULL;

				// verify_auth() function to authenticate user by group(s)
				if ($this->verify_auth($group) === true) {
					//if user group is authenticated set "login_authentication" session data
					$this->session->set_userdata('login_authentication', 'TRUE');
				}else{
					$this->session->set_flashdata('warning', $this->verify_auth($group));
					redirect($redirect_url);
					// $key = 'warning';
					// $message = $this->verify_auth($group);
				}
				// if ($message && $key) {
				// 	$this->session->set_flashdata($key, $message);
				// 	redirect($redirect_url);
				// }
			}
		}
	}

	// Verify user authentication
	// $group parameter can be name, ID, name array, ID array, or mixed array
	// Reference: http://benedmunds.com/ion_auth/#in_group
	protected function verify_auth($group)
	{
		if ( array_key_exists($this->mUri, $this->mPageAuth) && !$this->mPageAuth[$this->mUri] == 1 )
		{
			$this->session->set_flashdata('warning', 'You dont have permissions to access this page');
			redirect('dashboard');
		}
		// if ( !$this->ion_auth->in_group($group) ){

		// 	return 'You must be an Administrator to view this page';

		// }else{

		// 	return true;

		// }
	}

	// Render template
	protected function render($view_file, $layout = 'default')
	{
		
		// automatically generate page title
		if ( empty($this->mPageTitle) )
		{
			if ($this->mAction=='index')
				// set page title
		// $this->mPageTitle = humanize($this->mCtrler);
			else
				// set page title
		// $this->mPageTitle = humanize($this->mAction);
		}
		$this->data['page_title'] = $this->mPageTitle;
		$this->data['current_uri'] = uri_string();
		$this->data['uri'] = $this->mUri;
		$this->data['ctrler'] = $this->mCtrler;
		$this->data['action'] = $this->mAction;
		$this->data['user'] = $this->mUser;
		$this->data['menu'] = $this->mMenu;
		$this->data['page_auth'] = $this->mPageAuth;
		$this->data['body_class'] = $this->mBodyClass;
		$this->data['inner_view'] = $view_file;
		$this->load->view('_base/head', $this->data);
		$this->load->view('_layouts/'.$layout, $this->data);
		$this->load->view('_base/foot', $this->data);
	}
	public function check_database($config)
	{
	    // ini_set('display_errors', 'Off');
	    //  Check database connection if using mysqli driver
	    if( $config['dbdriver'] === 'mysqli' )
	    {
	        @$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
	        if( !$mysqli->connect_error )
	        {
	            @$mysqli->close();
	            return true;
	        }
	        @$mysqli->close();
	    }
	    return false;
	} 
	
}

require APPPATH."core/Admin_Controller.php";
