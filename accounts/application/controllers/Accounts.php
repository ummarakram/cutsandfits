<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends Admin_Controller {
	public function __construct() {
        parent::__construct(); 
    }   
    
	public function index() {

        $this->mBodyClass .= ' sidebar-collapse';
		
		$this->load->library('AccountList');
		
		$accountlist = new AccountList();
		$accountlist->Group = &$this->Group;
		$accountlist->Ledger = &$this->Ledger;
		$accountlist->only_opening = false;
		$accountlist->start_date = null;
		$accountlist->end_date = null;
		$accountlist->affects_gross = -1;
		$accountlist->start(0);

		$this->data['accountlist'] = $accountlist;
		$opdiff = $this->ledger_model->getOpeningDiff();
		$this->data['opdiff'] = $opdiff;
		// render page
		$this->render('accounts/index');
	}

	public function log()
	{
		// render log page
		$this->render('accounts/log');
	}
}


