<script type="text/javascript">
$(document).ready(function() {

	/* If use default email is checked then disable rest of the fields */
	$('#SettingEmailUseDefault').change(function() {
		if ($(this).is(':checked')) {
			$('#SettingEmailProtocol').prop('disabled', true);
			$('#SettingEmailHost').prop('disabled', true);
			$('#SettingEmailPort').prop('disabled', true);
			$('#SettingEmailTls').prop('disabled', true);
			$('#SettingEmailUsername').prop('disabled', true);
			$('#SettingEmailPassword').prop('disabled', true);
			$('#SettingEmailFrom').prop('disabled', true);
		} else {
			$('#SettingEmailProtocol').prop('disabled', false);
			$('#SettingEmailHost').prop('disabled', false);
			$('#SettingEmailPort').prop('disabled', false);
			$('#SettingEmailTls').prop('disabled', false);
			$('#SettingEmailUsername').prop('disabled', false);
			$('#SettingEmailPassword').prop('disabled', false);
			$('#SettingEmailFrom').prop('disabled', false);
		}
	});
	$('#SettingEmailUseDefault').trigger('change');
});
</script>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- ./col -->
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?= lang('settings_views_email_title'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="email form">

				<?php echo form_open(); ?>
					<div class="form-group" style="width: 25%;">
	                    <div class="input-group">
	                    	<label><input type="checkbox" class="skip" name="email_use_default" id="SettingEmailUseDefault"><?= lang('settings_views_email_checkbox_use_default_email_settings'); ?></label>
	                        <div class="input-group-addon">
	                            <i>
	                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('settings_views_email_use_default_email_settings_tooltip'); ?>">
	                                </div>
	                            </i>
	                        </div>
	                    </div>
	                    <!-- /.input group -->
	                </div>
	                <!-- /.form group -->
					<div class="row">
						<div class="col-md-4">
			                <?php
							//form_input('email_protocol');
							?>
							<div class="form-group">
								<label for="email_protocol"><?= lang('settings_views_email_label_email_protocol'); ?></label>
								<select name="email_protocol" id="SettingEmailProtocol" class="form-control">
							 		<option value="smtp" <?= ($settings->email_protocol == 'smtp') ? "selected" : "" ?>><?= lang('settings_views_email_email_protocol_option_smtp'); ?></option>
							 		<option value="mail" <?= ($settings->email_protocol == 'mail') ? "selected" : "" ?>><?= lang('settings_views_email_email_protocol_option_mail_function'); ?></option>
								</select>
							</div>
							<div class="form-group">
								<label for="smtp_host"><?= lang('settings_views_email_label_smtp_host'); ?></label>
								<input type="text" class="form-control" id="SettingEmailHost" name="smtp_host" value="<?= $settings->smtp_host ?>" placeholder="<?= lang('settings_views_email_label_smtp_host_placeholder'); ?>">
							</div>
							<div class="form-group">
								<label for="smtp_port"><?= lang('settings_views_email_label_smtp_port'); ?></label>
								<input type="text" class="form-control" id="SettingEmailPort" name="smtp_port" value="<?= $settings->smtp_port ?>" placeholder="<?= lang('settings_views_email_label_smtp_port_placeholder'); ?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="email_from"><?= lang('settings_views_email_label_smtp_email'); ?></label>
								<input type="text" class="form-control" id="SettingEmailFrom" value="<?= $settings->email_from ?>" name="email_from"  name="email_from" placeholder="<?= lang('settings_views_email_label_smtp_email_placeholder'); ?>">
							</div>
							<div class="form-group">
								<label for="smtp_password"><?= lang('settings_views_email_label_smtp_password'); ?></label>
								<input type="text" class="form-control" id="SettingEmailPassword" value="<?= $settings->smtp_password ?>" name="smtp_password" placeholder="<?= lang('settings_views_email_label_smtp_password_placeholder'); ?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="smtp_username"><?= lang('settings_views_email_label_smtp_username'); ?></label>
								<input type="text" class="form-control" id="SettingEmailUsername" value="<?= $settings->smtp_username ?>" name="smtp_username" placeholder="<?= lang('settings_views_email_label_smtp_username_placeholder'); ?>">
							</div>
							<div class="form-group" style="margin-top: 40px;">
								<label><input type="checkbox" class="skip" id="SettingEmailTls" name="smtp_tls" <?= ($settings->smtp_tls) ? "checked" : "" ?>><?= lang('settings_views_email_label_use_tls'); ?></label>
			                </div>
			                <!-- /.form group -->
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
				    <button type="submit" class="btn btn-primary pull-right"><?= lang('settings_views_email_btn_submit'); ?></button>
				</div>
				    <?= form_close(); ?>
				</div>
			
        </div>
      </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

