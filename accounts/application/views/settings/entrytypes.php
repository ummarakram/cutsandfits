<script>

    $(document).ready(function () {
        var oTable = $('#dynamic-table').dataTable({
            "aaSorting": [[3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "iDisplayLength": 10,
            'bProcessing': true, 'bServerSide': false,
            'sAjaxSource': '<?=base_url(); ?>account_settings/getAllET',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            }, 
            "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            ],
           
        });
              
    });

    jQuery(document).on("click", "#delete", function () {
        var num = jQuery(this).data("num");
        jQuery.ajax({
            type: "POST",
            url: "<?=base_url(); ?>" + "account_settings/entrytypes/delete",
            data: "id=" + encodeURI(num),
            cache: false,
            dataType: "json",
            success: function (data) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                if (data == 'true') {
                    toastr['success']("<?= lang('delete'); ?>: ", "<?= lang('settings_views_entrytypes_deleted'); ?>");
                }else{
                    toastr['error']("<?= lang('settings_views_entrytypes_deleted_error'); ?>");

                }
                $('#dynamic-table').DataTable().ajax.reload();
            }
        });
    });


</script>

<!-- ============= MODAL MODIFICA CLIENTI ============= -->
<div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titclienti"></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <p class="tips custip"></p>
                    <div class="row">
                        <form id="myForm" class="col s12">
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_label'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input name="et_label" id="et_label" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_name'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input name="et_name" id="et_name" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_description'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa  fa-user"></i>
                                        </div>
                                        <input name="description" id="description" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_numbering'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select name="numbering" id="numbering" class="form-control">
                                            <option value="1"><?= lang('settings_views_entrytypes_modal_numbering_option_1'); ?></option>
                                            <option value="2"><?= lang('settings_views_entrytypes_modal_numbering_option_2'); ?></option>
                                            <option value="3"><?= lang('settings_views_entrytypes_modal_numbering_option_3'); ?></option>
                                        </select>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_perfix'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input name="prefix" id="prefix" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_suffix'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input name="suffix" id="suffix" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_zero_padding'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input name="zero_padding" id="zero_padding" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?= lang('settings_views_entrytypes_modal_label_restrictions'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select id="restriction_bankcash" name="restriction_bankcash" class="form-control" required="required">
                                            <option value="1"><?= lang('settings_views_entrytypes_modal_restrictions_option_1'); ?></option>
                                            <option value="2"><?= lang('settings_views_entrytypes_modal_restrictions_option_2'); ?></option>
                                            <option value="3"><?= lang('settings_views_entrytypes_modal_restrictions_option_3'); ?></option>
                                            <option value="4"><?= lang('settings_views_entrytypes_modal_restrictions_option_4'); ?></option>
                                            <option value="5"><?= lang('settings_views_entrytypes_modal_restrictions_option_5'); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" id="footerClient1">
                  <!--    -->
            </div>
        </div>
    </div>
</div>
</div>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- ./col -->
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= lang('settings_views_entrytypes_title'); ?></h3>
            <button href="#clientmodal" class="add_c btn btn-primary pull-right">
                <i class="fa fa-plus-circle"><?= lang('settings_views_entrytypes_btn_add'); ?></i>
            </button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="responsive-table">
                <table class="display compact table table-bordered table-striped" id="dynamic-table">
                    <thead>
                        <tr>
                            <th><?= lang('settings_views_entrytypes_thead_label'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_name'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_description'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_prefix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_suffix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_zero_padding'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_actions'); ?></th>
                        </tr>
                    </thead>
                        
                    <tfoot>
                        <tr>
                            <th><?= lang('settings_views_entrytypes_tfoot_label'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_name'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_description'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_prefix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_suffix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_zero_padding'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_actions'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
      </div>
  </div>
  <!-- /.row -->
</section>

<script type="text/javascript">

    jQuery(".add_c").on("click", function (e) {
        $('#clientmodal').modal('show');

        jQuery('#et_label').val('');
        jQuery('#et_name').val('');
        jQuery('#description').val('');
        jQuery('#numbering').val('');
        jQuery('#prefix').val('');
        jQuery('#suffix').val('');
        jQuery('#zero_padding').val('');
        jQuery('#restriction_bankcash').val('');
            
        
        jQuery('#titclienti').html("<?= lang('settings_views_entrytypes_btn_add');?>");

        jQuery('#footerClient1').html('<button data-dismiss="modal" class="pull-left btn btn-default" type="button"><i class="fa fa-reply"></i><?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button role="submit" form="myForm" id="submit" class="btn btn-success" data-mode="add"><i class="fa fa-user"></i><?= lang('settings_views_entrytypes_modal_footer_btn_submit'); ?></button>');
    });

    jQuery(document).on("click", "#modify", function () {
        jQuery('#titclienti').html("<?= lang('settings_views_entrytypes_edit');?>");
        var num = jQuery(this).data("num");
            jQuery.ajax({
                type: "POST",
                url: "<?= base_url(); ?>account_settings/entrytypes/getByID",
                data: "id=" + encodeURI(num),
                cache: false,
                dataType: "json",
                success: function (data) {
                    jQuery('#et_label').val(data.label);
                    jQuery('#et_name').val(data.name);
                    jQuery('#description').val(data.description);
                    jQuery('#numbering').val(data.numbering);
                    jQuery('#prefix').val(data.prefix);
                    jQuery('#suffix').val(data.suffix);
                    jQuery('#zero_padding').val(data.zero_padding);
                    jQuery('#restriction_bankcash').val(data.restriction_bankcash);

                    jQuery('#footerClient1').html('<button data-dismiss="modal" class="pull-left btn btn-default" type="button"><i class="fa fa-reply"></i> <?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button role="submit" form="myForm" id="submit" class="btn btn-success" data-mode="modify" data-num="' + encodeURI(num) + '"><i class="fa fa-save"></i> <?= lang('settings_views_entrytypes_btn_save');?></button>')
                }
            });
        });

$( "#myForm" ).submit(function( event ) {
        event.preventDefault();
        var mode = jQuery('#submit').data("mode");
        var id = jQuery('#submit').data("num");

        //validate
        var valid = true;

        if (valid) {
            var url = "";
            var dataString = $('form').serialize();

            if (mode == "add") {
                url = "<?= base_url(); ?>" + "account_settings/entrytypes/add";
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('settings_views_entrytypes_added'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            } else {
                url = "<?= base_url(); ?>" + "account_settings/entrytypes/edit";
                dataString += "&id=" + encodeURI(id);
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('settings_views_entrytypes_update'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            }
        }
        return false;
    });
</script>