<!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

        
        <div class="col-lg-12 col-xs-12">
          <?php
          /* Check if BC Math Library is present */
          if (!extension_loaded('bcmath')) {
            echo '<div><div role="alert" class="alert alert-danger">'. lang('user_views_dashboard_error_alert_php_bc_math_lib_missing') .'</div></div>';
          }
          ?>
          <!-- small box -->
          <div class="row">
             <div class="col-xs-6">
               <div class="box box-success box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title" style="color: white;"><?=lang('dashboard_company_details');?></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                  <table class="table">
                    <tr>
                      <td><?php echo lang('dashboard_accountlist_table_name'); ?></td>
                      <td><?php echo ($this->mAccountSettings->name); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('settings_views_main_label_email'); ?></td>
                      <td><?php echo ($this->mAccountSettings->email); ?></td>

                    </tr>
                    <tr>
                      <td><?php echo lang('dashboard_company_role'); ?></td>
                      <td><?= ($this->ion_auth->get_users_groups()->row()->description); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('admin_cntrler_create_account_validation_currency'); ?></td>
                      <td><?php echo ($this->mAccountSettings->currency_symbol); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('settings_views_cf_label_fiscal_year'); ?></td>
                      <td><?php echo $this->functionscore->dateFromSql($this->mAccountSettings->fy_start) . ' to ' . $this->functionscore->dateFromSql($this->mAccountSettings->fy_end); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('settings_views_cf_label_status'); ?></td>
                      <?php
                        if ($this->mAccountSettings->account_locked == 0) {
                          echo '<td>' . lang('settings_views_cf_label_unlocked') . '</td>';
                        } else {
                          echo '<td class="error-text">' . lang('settings_views_cf_label_locked') . '</td>';
                        }
                      ?>
                    </tr>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
           </div>
           <div class="col-xs-6">
               <div class="box box-warning box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title" style="color: white;"><?= lang('dashboard_bc_summary'); ?></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                  <table class="table">
                  <?php
                    foreach ($ledgers as $ledger) {
                      echo '<tr>';
                      echo '<td>' . $ledger['name'] . '</td>';
                      echo '<td>' . $this->functionscore->toCurrency($ledger['balance']['dc'], $ledger['balance']['amount']) . '</td>';
                      echo '</tr>';
                    }
                  ?>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
           </div>
            <div class="col-xs-6">
               <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title" style="color: white;"><?= lang('dashboard_b_summary'); ?></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                  <table class="table">
                    <tr>
                      <td><?php echo ('Assets'); ?></td>
                      <td><?php echo $this->functionscore->toCurrency($accsummary['assets_total_dc'], $accsummary['assets_total']); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo (' Liabilities and Owners Equity'); ?></td>
                      <td><?php echo $this->functionscore->toCurrency($accsummary['liabilities_total_dc'], $accsummary['liabilities_total']); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo ('Income'); ?></td>
                      <td><?php echo $this->functionscore->toCurrency($accsummary['income_total_dc'], $accsummary['income_total']); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo ('Expense'); ?></td>
                      <td><?php echo $this->functionscore->toCurrency($accsummary['expense_total_dc'], $accsummary['expense_total']); ?></td>
                    </tr>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
           </div>
           <div class="col-xs-6 hide">
               <div class="box box-danger box-solid collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title" style="color: white;"><?= lang('recent_activity'); ?></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  <?php
                    if (count($logs) <= 0) {
                      echo lang('no_records_found');
                    } else {
                      echo '<table>';
                      foreach ($logs as $row => $data) {
                        echo '<tr>';
                        echo '<td>' . $this->functionscore->dateFromSql($data['date']) . '</td>';
                        echo '<td>' . ($data['message']) . '</td>';
                        echo '</tr>';
                      }
                      echo '</table>';
                    }
                  ?>
                  <!-- <span class="pull-right"><a href="<?= base_url('logs'); ?>">More</a></span> -->
                </div>
                <!-- /.box-body -->
              </div>
           </div>
          </div>

        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->