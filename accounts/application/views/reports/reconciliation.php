<script type="text/javascript">
$(document).ready(function() {

	$("#accordion").accordion({
		collapsible: true,
		<?php
			if ($options == false) {
				echo 'active: false';
			}
		?>
	});

	$(document.body).on("change","#ReportLedgerId",function(){
		if(this.value == 0){
			$('#ReportStartdate').prop('disabled', true);
			$('#ReportEnddate').prop('disabled', true);
		} else {
			$('#ReportStartdate').prop('disabled', false);
			$('#ReportEnddate').prop('disabled', false);
		}
	});
	$('#ReportLedgerId').trigger('change');

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	/* Setup jQuery datepicker ui */
	$('#ReportStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportEnddate").datepicker("option", "minDate", selectedDate);
			} else {
				$("#ReportEnddate").datepicker("option", "minDate", startDate);
			}
		}
	});
	$('#ReportEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportStartdate").datepicker("option", "maxDate", selectedDate);
			} else {
				$("#ReportStartdate").datepicker("option", "maxDate", endDate);
			}
		}
	});

	$('.recdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
	});

	var ReportLedgerId = $("#ReportLedgerId").select2({
		width:'100%',
		placeholder: "Please select a country"
	});
	// if( $('#ReportLedgerId').has('option').length < 2) {
	// 	ReportLedgerId.select2({
	// 		placeholder: 'qwerty'
	// 	});
	// }
});
</script>
<!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            	<div class="reconciliation form">
					<form action="<?= base_url(); ?>reports/reconciliation" method="post" accept-charset="utf-8">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><?= lang('ledger_acc_name'); ?></label>
									<select class="form-control" id="ReportLedgerId" name="ledger_id">
										<option></option>
										<?php foreach ($ledgers as $id => $ledger): ?>
											<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($this->input->post('ledger_id') == $id) ?'selected':''?>><?= $ledger; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('start_date'); ?></label>
				                    <div class="input-group">
										<input id="ReportStartdate" type="text" name="startdate" class="form-control">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?=lang('start_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('end_date') ;?></label>
				                    <div class="input-group">
										<input id="ReportEnddate" type="text" name="enddate" class="form-control">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?=lang('end_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
						</div>
						<div class="form-group">
							<label><input id="" type="checkbox" name="showall" class="form-control"><?= lang('show_all_entries');?></label>
						</div>
						<div class="form-group">
							<input type="reset" class="btn btn-primary pull-right" style="margin-left: 5px;" value="<?=lang('clear');?>">
							<input type="submit" name="submit_ledger" class="btn btn-primary pull-right" value="<?=lang('create_account_submit_button');?>">
						</div>
				</div>

				<?php if ($showEntries) { ?>
					<div class="subtitle">
						<?php echo $subtitle; ?>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-6">
							<table class="summary stripped table-condensed">
								<tr>
									<td class="td-fixwidth-summary"><?php echo ('Bank or cash account'); ?></td>
									<td>

										<?php
											if ($ledger_data['type'] == 1) {
												echo lang('yes');
											} else {
												echo lang('no');
											}
										?>
									</td>
								</tr>
								<tr>
									<td class="td-fixwidth-summary"><?php echo ('Notes'); ?></td>
									<td><?php echo ($ledger_data['notes']); ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">	
							<table class="summary stripped table-condensed">
								<tr>
									<td class="td-fixwidth-summary"><?php echo $opening_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency($op['dc'], $op['amount']); ?></td>
								</tr>
								<tr>
									<td class="td-fixwidth-summary"><?php echo $closing_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency($cl['dc'], $cl['amount']); ?></td>
								</tr>
								<tr>
									<td><?php echo ('Debit ') . $recpending_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency('D', $rp['dr_total']); ?></td>
								</tr>
								<tr>
									<td><?php echo ('Credit ') . $recpending_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency('C', $rp['cr_total']); ?></td>
								</tr>
							</table>
						</div>
					</div>
					<table class="stripped">
					<tr>
						<th><?php echo lang('entries_views_add_label_date'); ?></th>
						<th><?php echo lang('entries_views_add_label_number'); ?></th>
						<th><?php echo lang('entries_views_add_items_th_ledger'); ?></th>
						<th><?php echo lang('entries_views_index_th_type'); ?></th>
						<th><?php echo lang('entries_views_index_th_tag'); ?></th>
						<th><?php echo lang('entries_views_index_th_debit_amount'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
						<th><?php echo lang('entries_views_index_th_credit_amount'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
						<th><?php echo lang('reconciliation_data'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
					</tr>
					

					<?php
					/* Show the entries table */
					foreach ($entries as $row => $entry) {
						$et = $this->DB1->where('id', $entry['entrytype_id'])->get('entrytypes')->row_array();
						$entryTypeName = $et['name'];
						echo '<tr>';
						echo '<td>' . $this->functionscore->dateFromSql($entry['date']) . '</td>';
						echo '<td>' . ($this->functionscore->toEntryNumber($entry['number'], $entry['entrytype_id'])) . '</td>';
						echo '<td>' . ($this->functionscore->entryLedgers($entry['id'])) . '</td>';
						echo '<td>' . ($entryTypeName) . '</td>';
						echo '<td>' . $this->functionscore->showTag($entry['tag_id']) . '</td>';

						if ($entry['dc'] == 'D') {
							echo '<td>' . $this->functionscore->toCurrency('D', $entry['amount']) . '</td>';
							echo '<td>' . '</td>';
						} else if ($entry['dc'] == 'C') {
							echo '<td>' . '</td>';
							echo '<td>' . $this->functionscore->toCurrency('C', $entry['amount']) . '</td>';
						} else {
							echo '<td>' . lang('search_views_amounts_td_error') . '</td>';
							echo '<td>' . lang('search_views_amounts_td_error') . '</td>';
						}

						echo '<td>';
						echo form_hidden("ReportRec[".$row."][id]", $entry['eiid']);
						if ($entry['reconciliation_date']) {
							$data = array(
								'name' => "ReportRec[".$row."][recdate]",
								'class' => 'recdate',
								'value' => $this->functionscore->dateFromSql($entry['reconciliation_date']),

							 );
							echo form_input($data);
						} else {
							$data = array(
								'name' => "ReportRec[".$row."][recdate]",
								'class' => 'recdate',

							 );
							echo form_input($data);
						}
						echo '</td>';
						echo '</tr>';
					}
					?>
					</table>
					<br />

					<?php
						echo form_hidden('submitrec', 1);
					?>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary" value="<?=lang('Reconcile');?>">
					</div>
					<?= form_close(); ?>
				<?php } ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

