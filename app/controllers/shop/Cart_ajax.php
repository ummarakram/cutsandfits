<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_ajax extends MY_Shop_Controller
{

    function __construct() {
        parent::__construct();
        if ($this->Settings->mmode) { redirect('notify/offline'); }
    }

    function index() {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $this->data['page_title'] = lang('shopping_cart');
        $this->page_construct('pages/cart', $this->data);
    }

    function checkout() {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $this->data['customer'] = $this->site->getAwardPointsOfCustomer($this->session->userdata['email']);
        $this->data['addresses'] = $this->loggedIn ? $this->shop_model->getAddresses() : FALSE;
        $this->data['page_title'] = lang('checkout');
        $this->page_construct('pages/checkout', $this->data);
    }

    function add($product_id) {
        if ($this->input->is_ajax_request() || $this->input->post('quantity')) {
            $product = $this->site->getProductByID($product_id);
            $options = $this->shop_model->getProductVariants($product_id);
            $price = $product->promotion ? $product->promo_price : $product->price;
            $option = FALSE;
            if (!empty($options)) {
                if ($this->input->post('option')) {
                    foreach ($options as $op) {
                        if ($op['id'] == $this->input->post('option')) {
                            $option = $op;
                        }
                    }
                } else {
                    $option = array_values($options)[0];
                }
                $price = $option['price']+$price;
            }
            $selected = $option ? $option['id'] : FALSE;
            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
            $ctax = $this->site->calculateTax($product, $tax_rate, $price);
            $tax = $this->sma->formatDecimal($ctax['amount']);
            $price = $this->sma->formatDecimal($price);
            $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
            $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

            //if promotion with percentage is activated then we need to change the price of products.
            $category_id = $this->site->getCategoryIdByProductID($product_id);
            $how_much_off_cat = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
            $how_much_off_pro = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
            if($how_much_off_cat){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentage($unit_price, $how_much_off_cat);
            }else if($how_much_off_pro){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentage($unit_price, $how_much_off_pro);
            }


            $data = array(
                'id'            => $id,
                'product_id'    => $product->id,
                'qty'           => ($this->input->get('qty') ? $this->input->get('qty') : ($this->input->post('quantity') ? $this->input->post('quantity') : 1)),
                'name'          => $product->name,
                'code'          => $product->code,
                'price'         => $unit_price,
                'tax'           => $tax,
                'image'         => $product->image,
                'option'        => $selected,
                'options'       => !empty($options) ? $options : NULL
            );
            if ($this->cart->insert($data)) {
                //Check item on promotion or not --same product
                $this->site->checkBuyOneGetOne($product->id);
                //Check item on promotion or not
                $this->site->checkBuyOneGetOtherOne($product->id);
                //Check item on category promotion or not
                $this->site->checkBuyOneOnCategoryFree($product->id);

                if ($this->input->post('quantity')) {
                    if($this->session->userdata('product_on_promotion_ping')) {
                        $show_alert = 'you can select another product which has same value or less for free (Products With the same Mark)';
                        $this->session->unset_userdata('product_on_promotion_ping');
                    }else if($this->session->userdata('product_on_promotion_get')){
                        $show_alert = 'Congrts.! You get free product.';
                        $this->session->unset_userdata('product_on_promotion_get');
                    }
                    if($show_alert){
                        $this->session->set_flashdata('message', $show_alert);
                    }else{
                        $this->session->set_flashdata('message', lang('item_added_to_cart'));
                    }
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $this->cart->cart_data();
                }
            }
            $this->session->set_flashdata('error', lang('unable_to_add_item_to_cart'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }



    public function checkProductExistInWarehouse($product_id, $warehouse_id){
        foreach($warehouse_id as $item){
            $this_product_exist_wh[] = $this->site->getWarehouseProduct($item , $product_id);
        }
        if(array_filter($this_product_exist_wh)){
            return true;
        }else{
            return false;
        }
    }


    public function all_item_count(){
        $total = 0;
        if ($this->cart->total_items() > 0){
            foreach ($this->cart->contents() AS $item){
                $total = $item['qty'] + $total;
            }
        }
        return $total;
    }


    function specificProductInCart($product_id){
        $is_on_adding_record = $this->shop_model->getUpcomingPromotions('onAddingProduct', $product_id);

        if(!empty($is_on_adding_record)) {
            foreach ($is_on_adding_record as $row) {
                $item_on_base = $row->item_id;
                $triggerOnItem = $row->triggerOnItem;
                break;
            }
            if($item_on_base == $product_id && $triggerOnItem == 1){
                $get_upselling_product = $this->shop_model->getItemBasePromotion('onAddingProduct', $product_id);
                $img_path = $this->shop_model->getProductImagePath($get_upselling_product[0]->item_id);
                $product_real_price = $this->sma->convertMoney($img_path[0]->price);
                //print_r($get_upselling_product); die;

                $data = array(
                    "id" => $row->id,
                    "popup_product_id" => $get_upselling_product[0]->item_id,
                    "name"  => $row->name,
                    "title" => $row->title,
                    "description" => $row->description,
                    "type" => $row->type,
                    "isMoreThanOne" => $row->isMoreThanOne,
                    "disqualifiedRemoval" => $row->disqualifiedRemoval,
                    "isRedemptionCheck"   => $row->isRedemptionCheck,
                    "redemptionLimit"     => $row->redemptionLimit,
                    "isCartTotal"       => $row->isCartTotal,
                    "minAmount"       => $row->minAmount,
                    "maxAmount"       => $row->maxAmount,
                    "isDateRange"       => $row->isDateRange,
                    "startDate"       => $row->startDate,
                    "endDate"       => $row->endDate,
                    "isTimeRange"       => $row->isTimeRange,
                    "startTime"       => $row->startTime,
                    "endTime"       => $row->endTime,
                    "triggerOnItem"       => $row->triggerOnItem,
                    "itemQuantity"       => $row->itemQuantity,
                    "status" => $row->status,
                    "inDaysOnly" => $row->inDaysOnly,
                    "days" => $row->days,
                    "upselling_id" => $row->upselling_id,
                    "product_image_path" => $img_path[0]->image,
                    "product_real_price" => 'AED '.$product_real_price,
                );

                $data[] = $data;
                echo json_encode($data);
                exit();
            }
        }


    }

    function update($data = NULL) {
        if (is_array($data)) {
            return $this->cart->update($data);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {
                $item = $this->cart->get_item($rowid);
                $product = $this->site->getProductByID($item['product_id']);
                $options = $this->shop_model->getProductVariants($product->id);
                // $option = $options ? array_values($options)[0] : FALSE;
                $price = $product->promotion ? $product->promo_price : $product->price;
                // $price = $options ? ($option['price']+$price) : $price;
                // $selected = $option ? $option['id'] : FALSE;

                if ($option = $this->input->post('option')) {
                    foreach($options as $op) {
                        if ($op['id'] == $option) {
                            $price = $price + $op['price'];
                        }
                    }
                }

                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
                // $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

                // $options = $this->cart->product_options($rowid);
                // $price = $item['base_price'];
                
                $data = array(
                    'rowid' => $rowid,
                    'price' => $price,
                    'tax' => $tax,
                    'qty' => $this->input->post('qty', TRUE),
                    'option' => $this->input->post('option') ? $this->input->post('option', TRUE) : FALSE,
                    // 'options' => $this->cart->product_options($rowid)
                );
                if ($this->cart->update($data)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_updated')));
                }
            }
        }
    }

    function remove($rowid = NULL) {
        if ($rowid) {
            return $this->cart->remove($rowid);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {

                    $item = $this->cart->get_item($rowid);
                    $is_promotion = $this->site->promotionAddToCart($item['product_id']);
                    if(!empty($is_promotion)){
                        foreach ($is_promotion as $row) {
                            $data_how_buy = $row->how_many_buy;
                            $data_how_free = $row->how_many_free;
                            $how_many_this_product_incart = $this->site->specific_item_count($item['product_id']);

                            if($how_many_this_product_incart - 2 < $data_how_buy){
                                $this->site->specific_promotional_item_delete($item['product_id']);
                            }
                        }
                    }
                if ($this->cart->remove($rowid)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_item_deleted')));
                }
            }
        }
    }

    function destroy() {
        if ($this->input->is_ajax_request()) {
            if ($this->cart->destroy()) {
                $this->session->set_flashdata('message', lang('cart_items_deleted'));
                $this->sma->send_json(array('redirect' => base_url()));
            } else {
                $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured')));
            }
        }
    }

    function add_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->getWishlist(TRUE) >= 10) {
            $this->sma->send_json(array('status' => lang('warning'), 'message' => lang('max_wishlist'), 'level' => 'warning'));
        }
        if ($this->shop_model->addWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('added_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('info'), 'message' => lang('product_exists_in_wishlist'), 'level' => 'info'));
        }
    }

    function remove_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->removeWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('removed_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured'), 'level' => 'error'));
        }
    }

}
