<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (DEMO && ($m == 'main' && $v == 'index')) { ?>
<div class="page-contents padding-top-no">
    <div class="container">
        <div class="alert alert-info margin-bottom-no">
            <p></p>
        </div>
    </div>
</div>
<?php } ?>

<section class="footer">
    <div class="container padding-bottom-md">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title-footer"><span><?= lang('about_us'); ?></span></div>
                <p>
                    <?= $shop_settings->description; ?> <a href="<?= site_url('page/'.$shop_settings->about_link); ?>"><?= lang('read_more'); ?></a>
                </p>
                <p>
                    <i class="fa fa-phone"></i> <span class="margin-left-md"><?= $shop_settings->phone; ?></span>
                    <i class="fa fa-envelope margin-left-xl"></i> <span class="margin-left-md"><?= $shop_settings->email; ?></span>
                </p>
                <ul class="list-inline">
                    <li><a href="<?= site_url('page/'.$shop_settings->privacy_link); ?>"><?= lang('privacy_policy'); ?></a></li>
                    <li><a href="<?= site_url('page/'.$shop_settings->terms_link); ?>"><?= lang('terms_conditions'); ?></a></li>
                    <li><a href="<?= site_url('page/'.$shop_settings->contact_link); ?>"><?= lang('contact_us'); ?></a></li>
                    <li><a href="<?= site_url('page/shipping-policy'); ?>">Shipping Policy</a></li>
                </ul>
            </div>

            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span><?= lang('payment_methods'); ?></span></div>
                <p><?= $shop_settings->payment_text; ?></p>
                <img class="img-responsive" src="<?= $assets; ?>/images/payment-methods.png" alt="Payment Methods">
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span><?= lang('follow_us'); ?></span></div>
                <p><?= $shop_settings->follow_text; ?></p>
                <ul class="follow-us">
                    <?php if (!empty($shop_settings->facebook)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } if (!empty($shop_settings->twitter)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } if (!empty($shop_settings->google_plus)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->google_plus; ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php } if (!empty($shop_settings->instagram)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright line-height-lg">
                &copy; <?= date('Y'); ?> <?= $shop_settings->shop_name; ?>. <?= lang('all_rights_reserved'); ?>
            </div>
            <ul class="list-inline pull-right line-height-md">
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue" data-color="blue"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue-grey" data-color="blue-grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-brown" data-color="brown"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-cyan" data-color="cyan"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-green" data-color="green"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-grey" data-color="grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-purple" data-color="purple"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-orange" data-color="orange"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-pink" data-color="pink"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-red" data-color="red"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-teal" data-color="teal"><i class="fa fa-square"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<a href="#" class="back-to-top text-center" onclick="$('body,html').animate({scrollTop:0},500); return false">
    <i class="fa fa-angle-double-up"></i>
</a>
</section>
<?php if (!get_cookie('shop_use_cookie') && get_cookie('shop_use_cookie') != 'accepted' && !empty($shop_settings->cookie_message)) { ?>
<div class="cookie-warning">
    <div class="bounceInLeft alert alert-info">
        <a href="<?= site_url('main/cookie/accepted'); ?>" class="close">&times;</a>
        <p>
            <?= $shop_settings->cookie_message; ?>
            <?php if (!empty($shop_settings->cookie_link)) { ?>
            <a href="<?= site_url('page/'.$shop_settings->cookie_link); ?>"><?= lang('read_more'); ?></a>
            <?php } ?>
        </p>
    </div>
</div>
<?php } ?>
<script type="text/javascript">
    var m = '<?= $m; ?>', v = '<?= $v; ?>', products = {}, filters = <?= isset($filters) && !empty($filters) ? json_encode($filters) : '{}'; ?>, shop_color, shop_grid, sorting;
    var cart = <?= isset($cart) && !empty($cart) ? json_encode($cart) : '{}' ?>;
    var site = {base_url: '<?= base_url(); ?>', site_url: '<?= site_url('/'); ?>', shop_url: '<?= shop_url(); ?>', csrf_token: '<?= $this->security->get_csrf_token_name() ?>', 
    csrf_token_value: '<?= $this->security->get_csrf_hash() ?>', settings: {display_symbol: '<?= $Settings->display_symbol; ?>', symbol: '<?= $Settings->symbol; ?>', decimals: <?= $Settings->decimals; ?>, thousands_sep: '<?= $Settings->thousands_sep; ?>', decimals_sep: '<?= $Settings->decimals_sep; ?>', order_tax_rate: false , products_page: <?= $shop_settings->products_page ? 1 : 0; ?>}}

    var lang = {};
    lang.page_info = '<?= lang('page_info'); ?>';
    lang.cart_empty = '<?= lang('empty_cart'); ?>';
    lang.item = '<?= lang('item'); ?>';
    lang.items = '<?= lang('items'); ?>';
    lang.unique = '<?= lang('unique'); ?>';
    lang.total_items = '<?= lang('total_items'); ?>';
    lang.total_unique_items = '<?= lang('total_unique_items'); ?>';
    lang.tax = '<?= lang('tax'); ?>';
    lang.shipping = '<?= lang('shipping'); ?>';
    lang.total_w_o_tax = '<?= lang('total_w_o_tax'); ?>';
    lang.product_tax = '<?= lang('product_tax'); ?>';
    lang.order_tax = '<?= lang('order_tax'); ?>';
    lang.total = '<?= lang('total'); ?>';
    lang.grand_total = '<?= lang('grand_total'); ?>';
    lang.reset_pw = '<?= lang('forgot_password?'); ?>';
    lang.type_email = '<?= lang('type_email_to_reset'); ?>';
    lang.submit = '<?= lang('submit'); ?>';
    lang.error = '<?= lang('error'); ?>';
    lang.add_address = '<?= lang('add_address'); ?>';
    lang.update_address = '<?= lang('update_address'); ?>';
    lang.fill_form = '<?= lang('fill_form'); ?>';
    lang.already_have_max_addresses = '<?= lang('already_have_max_addresses'); ?>';
    lang.send_email_title = '<?= lang('send_email_title'); ?>';
    lang.message_sent = '<?= lang('message_sent'); ?>';
</script>
<script src="<?= $assets; ?>js/libs.min.js"></script>
<script src="<?= $assets; ?>js/scripts.min.js"></script>
<script type="text/javascript">
<?php if ($message || $warning || $error || $reminder) { ?>
$(document).ready(function() {
    <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($message))); ?>');
    <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($warning))); ?>', 'warning');
    <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($error))); ?>', 'error', 1);
    <?php } if ($reminder) { ?>
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($reminder))); ?>', 'info');
    <?php } ?>
});
<?php } ?>
</script>

<script type="text/javascript">

    $("#results").on("mouseover",".grid .custom_pro_list_img_bucket", function(){
        var qv_id = this.id;
        $("#results .grid .product-image_"+qv_id).addClass("grid_product_quick");
        $("#results .grid .product-image_"+qv_id+" a .quickview").removeClass('hide');

    });
    $("#results").on("mouseout",".grid .custom_pro_list_img_bucket", function(){
        var qv_id = this.id;
        $("#results .grid .product-image_"+qv_id).removeClass("grid_product_quick");
        $("#results .grid .product-image_"+qv_id+" a .quickview").addClass('hide');
    });


    function shareit(){
        var url= $('.link_viewfull_pro').attr('href'); //Set desired URL here
        var img= $('#pro_quickview_img').attr('src'); //Set Desired Image here

        var totalurl=encodeURIComponent(url+'?img='+img);

        window.open ('http://www.facebook.com/sharer.php?u='+totalurl,'','width=500, height=500, scrollbars=yes, resizable=no');

    }

    //To check the product BuyOneGetOne
/*    $(document).on('click', '.add-to-cart', function(e) {
        var product_id_ = $(this).attr("data-id");
        if(product_id_){
            $.ajax({
                url: site.site_url+'cart/checkBuyOneGetOne/'+product_id_,
                type: 'GET',
                dataType: 'json',
                data: {
                },
            });
        }
    });*/
    //End of To check the product BuyOneGetOne

    //To check Promotion on specific product add to cart
    $(document).on('click', '.add-to-cart', function(e) {
        var product_id_ = $(this).attr("data-id");
        if(product_id_){
            $.ajax({
                url: site.site_url+'cart/specificProductInCart/'+product_id_,
                type: 'GET',
                dataType: 'json',
                data: {
                },
            }).done(
                function(data){
                    //alert(JSON.stringify(data));
                    $("#itembasepromotion .modal-title").html(data.title);
                    $('#upsell_img_path').attr('src', site.site_url+'assets/uploads/'+data.product_image_path);
                    $("#itembasepromotion .modal_custom_padding.name").html(data.name);
                    $("#itembasepromotion .modal_promotion.description").html(data.description);
                    $("#itembasepromotion .modal_promotion_price.price .innerside_price").html(data.product_real_price);
                    $("#itembasepromotion .modal-footer .custom_onbase_ofproduct").attr('data-id',data.popup_product_id);
                    $("#itembasepromotion").modal('show');
                });
        }
    });

    $(document).on('click', '.custom_onbase_ofproduct , .quick_view_cart', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var cart = $('.shopping-cart:visible');
        $.ajax({url: site.site_url+'cart/add/'+id, type: 'GET', dataType: 'json', data: {
            //ty: qty_input.val()
        }})
            .done(function(data) {
                cart = data; update_mini_cart(data);

                var is_cat_on_prom = JSON.stringify(data.cat_on_promotion);
                is_cat_on_prom = is_cat_on_prom.replace('"' , '');
                is_cat_on_prom = is_cat_on_prom.replace('"' , '');
                if(is_cat_on_prom != '' && is_cat_on_prom != 'null' ){
                    swal("",
                        is_cat_on_prom,
                        "success");
                }

            });

        $("#itembasepromotion").modal('hide');
        $("#pro_quickview").modal('hide');
    });


    //Product Quick view popup
    $(document).on('click', '.quickview_pop', function(e) {
        e.preventDefault();
        var pro_id = $(this).attr('data-id');
        var cart = $('.shopping-cart:visible');
        $.ajax({url: site.site_url+'shop/product_quickview/'+pro_id, type: 'GET', dataType: 'json', data: {
            //ty: qty_input.val()
        }}).done(function(data) {
                //alert(JSON.stringify(data));
                //alert(JSON.stringify(data.free_shipment));
                $("#pro_quickview .modal-title").html(data.name+' ('+ data.code + ')' );

                if($.trim(data.free_shipment) == 1){
                    $("#pro_quickview .free-shipping-cont .free-shipping-cont_inner .free_shipng").html("<strong class='green-text'>FREE SHIPPING</strong>");
                }else{
                    $("#pro_quickview .free-shipping-cont .free-shipping-cont_inner .free_shipng").html("<strong class='green-text'></strong>");
                }

                if(data.promotion == 0 || data.promotion == null ){
                    $(".save_amount_sec").hide();
                    $("#pro_quickview .price_sec_contaire").html(data.price);
                }else{
                    $(".save_amount_sec").show();
                    $("#pro_quickview .price_sec_contaire").html(data.promo_price);
                    $("#pro_quickview .save_amount_sec .was").html(data.price);
                    $("#pro_quickview .save_amount_sec .saved .noWrap").html(data.save_amount);
                }
                if(!$.trim(data.details)){
                    $("#pro_quickview .description_sec").hide();
                }else{
                    $("#pro_quickview .description_sec").show();
                    $("#pro_quickview .description_sec p").html(data.details);
                }

                if(!$.trim(data.variants)) {
                    $("#pro_quickview .variants").hide();
                }else{
                    $("#pro_quickview .variants").show();
                    $("#pro_quickview .variants .variants_space").html(data.variants);
                }

                $("#pro_quickview #twitter_share_btn").attr('href', 'https://twitter.com/share?url='+site.site_url+'product/'+data.slug+'/&amp;text='+data.name+'&amp;hashtags='+data.name );

                $("#pro_quickview #google_share_btn").attr('href', 'https://plus.google.com/share?url='+site.site_url+'product/'+data.slug);

                $("#pro_quickview .cart_mainarea span.text_span_stock span.amount_span_stock").html(data.quantity);
                $("#pro_quickview .link_viewfull_pro").attr('href', site.site_url+'product/'+data.slug);
                $('#pro_quickview_img').attr('src', site.site_url+'assets/uploads/'+data.image);
                $('#pro_quickview .cart_mainarea button.cus_quick_addtocart_btn.add-to-cart').attr('data-id', pro_id);
                $("#pro_quickview").modal('show');
            });

    });
    //End of Product Quick view popup

</script>



<div id="itembasepromotion" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body nopadding">
                <img id="upsell_img_path" width="598" src="<?= base_url('assets/uploads/no_image.png'); ?>" />

                <h4 class="modal_custom_padding name"></h4>
                <span class="modal_promotion description"></span>
                <span class="modal_promotion_price price"><strong class="innerside_price"></strong></span>

            </div>
            <div class="modal-footer">
                <div class="btn add-to-cart btn-success cus_btn_prom custom_onbase_ofproduct" data-id="<?= $upcoming_promotions[0]->item_id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                <button type="button" class="btn btn-default cus_btn_prom" data-dismiss="modal">No Thanks</button>
            </div>
        </div>

    </div>
</div>



<!-- Popup for Quick View -->
<div id="pro_quickview" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
                <div class="free-shipping-cont pull-right">
                    <div class="free-shipping-cont_inner fs-ab-black" data-default="true">
                        <div class="fbs-eligible free_shipng"></div>
                    </div>
                </div>
                <img src="https://hcplteenscene.files.wordpress.com/2011/10/four-stars.png" style="width: 74px; height: 14px; padding: 0px;">
                <i class="fa fa-chevron-down"></i> <a class="review_clrlink" href="javascript:void(0)">6 Reviews</a>
            </div>
            <div class="modal-body custom_popupbody">
                <div class="custom_area1 border-right">
                    <div class="image_bucket">
                        <img class="img-responsive " id="pro_quickview_img" src="<?= base_url('assets/uploads/no_image.png'); ?>" />
                    </div>

                    <div class="opposite-direction band socialIconsWrapper">
                        <ul class="social_icons_list">
                            <!-- Facebook -->
                            <a id="facebook_share_btn" href="" onclick="shareit()" target="_blank">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                <!--<img src="<?/*= site_url('assets/uploads/facebook.png') */?>" alt="Facebook" />-->
                            </a>

                            <!-- Twitter -->
                            <a id="twitter_share_btn" href="" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>

                            <!-- Google+ -->
                            <a id="google_share_btn" href="" target="_blank">
                                <i class="fa fa-google-plus-official" aria-hidden="true"></i>
                            </a>

                        </ul>
                    </div>
                </div>

                <div class="custom_area2">
                    <div class="price_sec">
                        <h3 class="price_sec_contaire">2,449.00 AED</h3>

                        <div class="mrgin-left-10 save_amount_sec">
                            <span class="was">3,099.00 AED </span>
                            <span class="saved">&nbsp; - You Save&nbsp;<span class="noWrap">650.00 AED</span></span>
                        </div>

                    </div>
                    <hr/>

                    <div class="cat_attributes mrgin-left-10 hide">
                        <strong>Color: </strong> &nbsp;(3 More)
                        <select class="form-control" style="width: 50%;">
                            <option value="red">Red</option>
                            <option value="blue">Blue</option>
                            <option value="pink">Pink</option>
                            <option value="white">White</option>
                        </select>
                    </div>

                    <div class="variants mrgin-left-10">
                        <strong>Size: </strong> &nbsp;(6 More)
                        <div class="variants_space" style="width: 50%;">
                        </div>
                    </div>

                    <div class="special_instructions mrgin-left-10">
                        <strong>Instructions: </strong><br/>
                        <textarea placeholder="Write your Instructions here..." class="form-control special_inc_textarea"></textarea>
                    </div>


                    <hr/>
                    <div class="description_sec mrgin-left-10">
                        <h4>PRODUCT INFORMATION</h4>
                        <p>The Bosch Whole fruit juicer have comfort for every users. This fruit juicer stainless steel micro filter basket for best juice extraction...</p>
                    </div>
                    <a class="link_viewfull_pro mrgin-left-10" href="#">View full product details</a>

                    <div class="cart_mainarea pull-right">
                        <div class="unit-labels leftInStock">
                            <b class="txtcolor-alert xleft">
                                <span class="text_span_stock">Order now, only <span class="amount_span_stock">5</span> left in stock!</span>
                            </b>
                        </div>
                        <button  type="submit" class="btn btn-lg btn-success cus_quick_addtocart_btn add-to-cart quick_view_cart" data-id=""><i class="fa fa-shopping-cart padding-right-md"></i> Add to Cart</button>
                    </div>

                </div>

            </div>
        </div>
        <!-- end of modal content -->
    </div>
</div>
<!-- Popup for Quick View -->


</body>
</html>
