<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($slider)) { ?>
    <section class="slider-container">
        <div class="container-fluid">
            <div class="row">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators margin-bottom-sm">
                        <?php
                        $sr = 0;
                        foreach ($slider as $slide) {
                            if (!empty($slide->image)) {
                                echo '<li data-target="#carousel-example-generic" data-slide-to="' . $sr . '" class="' . ($sr == 0 ? 'active' : '') . '"></li> ';
                            }
                            $sr++;
                        }
                        ?>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <?php
                        $sr = 0;
                        foreach ($slider as $slide) {
                            if (!empty($slide->image)) {
                                echo '<div class="item' . ($sr == 0 ? ' active' : '') . '">';
                                if (!empty($slide->link)) {
                                    echo '<a href="' . $slide->link . '">';
                                }
                                echo '<img src="' . base_url('assets/uploads/' . $slide->image) . '" alt="">';
                                if (!empty($slide->caption)) {
                                    echo '<div class="carousel-caption">' . $slide->caption . '</div>';
                                }
                                if (!empty($slide->link)) {
                                    echo '</a>';
                                }
                                echo '</div>';
                            }
                            $sr++;
                        }
                        ?>
                    </div>

                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only"><?= lang('prev'); ?></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only"><?= lang('next'); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<section class="page-contents">
    <div class="container">

        <!-- Start of Featured Products -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="h2heading_landing_page text-center">
                    <?= lang('featured_products'); ?>
                </h2>
                <hr/>
                <div class="row">
                    <div class="col-xs-9"></div>
                    <?php
                    if (count($featured_products) > 5) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right hidden-xs">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-example"
                                   data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-example"
                                   data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div id="carousel-example" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($featured_products, 5) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="col-sm-6 col-md-3 custom_block_width">
                                            <div class="product custom_product_tab" style="z-index: 1;">
                                                <div class="details" style="transition: all 100ms ease-out 0s;">
                                                    <?php
                                                    if ($fp->promotion) {
                                                        ?>
                                                        <span class="badge badge-right theme"><?= lang('promo'); ?></span>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    $this->site->compress(FCPATH . "assets/uploads/" . $fp->image, FCPATH . "assets/uploads/" . $fp->image, 90);
                                                    ?>
                                                    <img class="featured_img"
                                                         src="<?= base_url('assets/uploads/' . $fp->image); ?>" alt="">

                                                    <div class="image_overlay cus_overlay"></div>
                                                    <!--<div class="btn add-to-cart cus_add_to_cart" data-id="<?/*= $fp->id; */ ?>"><i class="fa fa-shopping-cart"></i> <?/*= lang('add_to_cart'); */ ?></div>-->
                                                    <div class="btn btn-primary quickview quickview_pop cus_add_to_cart"
                                                         data-id="<?= $fp->id; ?>">QUICK VIEW
                                                    </div>
                                                    <div class="stats-container cus_stats_container">
                                                        <!--<span class="product_price">
                                                            <?php
                                                        /*                                                            if ($fp->promotion) {
                                                                                                                        echo '<del class="text-red">'.$this->sma->convertMoney($fp->price).'</del><br>';
                                                                                                                        echo $this->sma->convertMoney($fp->promo_price);
                                                                                                                    } else {
                                                                                                                        echo $this->sma->convertMoney($fp->price);
                                                                                                                    }
                                                                                                                    */ ?>
                                                        </span>-->
                                                        <span class="product_name landing_pro_name">
                                                            <h5><a class="blackcolor"
                                                                   href="<?= site_url('product/' . $fp->slug); ?>"><?= substr($fp->name, 0, 40); ?></a></h5>
                                                        </span>
                                                        <a href="<?= site_url('category/' . $fp->category_slug); ?>"
                                                           class="link"><?= $fp->category_name; ?></a>
                                                        <!--                                                        <span class="ranking">
                                                            <img src="<?/*= base_url()*/ ?>assets/uploads/four-stars.png" style="width: 74px; height: 14px; padding: 0px;">
                                                        </span>-->

                                                        <span class="product_price custom_product_pricttab">
                                                            <?php
                                                            if ($fp->promotion) {
                                                                echo '<del class="text-red was">' . $this->sma->convertMoney($fp->price) . ' AED</del><br>';
                                                                echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($fp->promo_price) . ' AED</span>';
                                                            } else {
                                                                echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($fp->price) . ' AED</span>';
                                                            }
                                                            ?>
                                                        </span>

                                                        <strong>
                                                            <p><?= $fp->free_shipment == 1 ? 'FREE Shipping' : '' ?> </p>
                                                        </strong>
                                                        <!-- <a href="<?/*= site_url('category/'.$fp->category_slug); */ ?>" class="link"><?/*= $fp->category_name; */ ?></a>
                                                        <?php
                                                        /*                                                        if ($fp->brand_name) {
                                                                                                                    */ ?>
                                                            <span class="link">-</span>
                                                            <a href="<?/*= site_url('brand/'.$fp->brand_slug); */ ?>" class="link"><?/*= $fp->brand_name; */ ?></a>
                                                            --><?php
                                                        /*                                                        }
                                                                                                                */ ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-xs-12">
                    <?php foreach ($landing_page_blocks as $landing_page_block) { ?>
                        <h2 class="h2heading_landing_page text-center">
                            <?= $landing_page_block->title; ?>
                        </h2>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-9"></div>
                            <?php
                            $get_products = $this->shop_model->getAllProductsAgainstBlockId($landing_page_block->id, $landing_page_block->number_of_products);
                            //print_r($get_products);
                            if (count($get_products) > 5) { ?>
                                <div class="col-xs-3">
                                    <div class="controls pull-right hidden-xs">
                                        <a class="left fa fa-chevron-left btn btn-xs btn-default"
                                           href="#carousel-example-<?= $landing_page_block->id ?>"
                                           data-slide="prev"></a>
                                        <a class="right fa fa-chevron-right btn btn-xs btn-default"
                                           href="#carousel-example-<?= $landing_page_block->id ?>"
                                           data-slide="next"></a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>


                        <?php
                        /*$products = $this->site->getAllProducts();
                        foreach($products as $product){
                            $this->site->compress(FCPATH."assets/uploads/".$product, FCPATH."assets/uploads2/".$product, 90);
                            //print_r($product);
                        }*/
                        ?>


                        <div id="carousel-example-<?= $landing_page_block->id ?>" class="carousel slide"
                             data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php
                                $r = 0;
                                foreach (array_chunk($get_products, 5) as $fps) { ?>
                                    <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                        <div class="featured-products">
                                            <?php foreach ($fps as $fp) { ?>

                                                <div class="col-sm-6 col-md-3 custom_block_width">
                                                    <div class="product custom_product_tab" style="z-index: 1;">

                                                        <div class="details" style="transition: all 100ms ease-out 0s;">
                                                            <?php
                                                                $buy1get1_offer = $this->shop_model->findOfferedProducts('buyOneGetOne',$fp->id);
                                                                $buy1get1_other_offer = $this->shop_model->findOfferedProducts('buyOneGetOtherProductFree',$fp->id);
                                                                $buy1get1_cat_offer = $this->shop_model->findOfferedProducts('buyOneGetCategoryFree',$fp->id,$fp->category_id);
                                                                $buy_cat_with_percentage = $this->shop_model->findOfferedProducts('buyCategoryWithPercentage',$fp->id,$fp->category_id);
                                                                $buy_product_with_percentage = $this->shop_model->findOfferedProducts('buyProductsWithPercentage',$fp->id,$fp->category_id);
                                                            if($buy1get1_offer) {
                                                                echo $buy1get1_offer;
                                                            }elseif ($buy1get1_other_offer){
                                                                echo $buy1get1_other_offer;
                                                            }elseif ($buy1get1_cat_offer){
                                                                echo $buy1get1_cat_offer;
                                                            }elseif($buy_cat_with_percentage){
                                                                echo $buy_cat_with_percentage;
                                                            }elseif ($buy_product_with_percentage){
                                                                echo $buy_product_with_percentage;
                                                            }
                                                            $this->site->compress(FCPATH . "assets/uploads/" . $fp->image, FCPATH . "assets/uploads/" . $fp->image, 90);
                                                            ?>
                                                            <img class="featured_img"
                                                                 src="<?= base_url('assets/uploads/' . $fp->image); ?>"
                                                                 alt="">

                                                            <div class="image_overlay cus_overlay"></div>
                                                            <div class="btn btn-primary quickview quickview_pop cus_add_to_cart"
                                                                 data-id="<?= $fp->id; ?>">QUICK VIEW
                                                            </div>
                                                            <div class="stats-container cus_stats_container">

                                                <span class="product_name landing_pro_name">
                                                    <h5>
                                                        <a class="blackcolor"
                                                           href="<?= site_url('product/' . $fp->slug); ?>"><?= substr($fp->name, 0, 50); ?></a>
                                                    </h5>
                                                </span>
                                                                <a href="<?= site_url('category/' . $fp->category_slug); ?>"
                                                                   class="link"><?= $fp->category_name; ?></a>

                                                                <span class="product_price custom_product_pricttab">
                                                    <?php
                                                    if ($fp->promotion) {
                                                        echo '<del class="text-red was">' . $this->sma->convertMoney($fp->price) . ' AED</del><br>';
                                                        echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($fp->promo_price) . ' AED</span>';
                                                    }else if(!empty($buy_cat_with_percentage)){
                                                        $how_much_off = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$fp->id,$fp->category_id);
                                                        if($how_much_off){
                                                            $custom_promo_price =  $this->site->calCulateProductPriceWithPromotionPercentage($fp->price, $how_much_off);
                                                            echo '<del class="text-red was">' . $this->sma->convertMoney($fp->price) . ' AED</del><br>';
                                                            echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($custom_promo_price) . ' AED</span>';
                                                        }
                                                    }else if(!empty($buy_product_with_percentage)){
                                                        $how_much_off = $this->site->getPromotionPercentage('buyProductsWithPercentage',$fp->id,$fp->category_id);
                                                        if($how_much_off){
                                                            $custom_promo_price =  $this->site->calCulateProductPriceWithPromotionPercentage($fp->price, $how_much_off);
                                                            echo '<del class="text-red was">' . $this->sma->convertMoney($fp->price) . ' AED</del><br>';
                                                            echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($custom_promo_price) . ' AED</span>';
                                                        }
                                                    } else {
                                                        echo '<span class="is_price_tab is_souq_price_color">' . $this->sma->convertMoney($fp->price) . ' AED</span>';
                                                    }
                                                    ?>
                                                </span>


                                                                <strong>
                                                                    <p><?= $fp->free_shipment == 1 ? 'FREE Shipping' : '' ?> </p>
                                                                </strong>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php } ?>

                                        </div>
                                    </div>

                                    <?php
                                    $r++;
                                }
                                ?>
                            </div>
                        </div>

                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</section>
<style type="text/css">
    .ribbon{
        left: 0px !important;
    }
</style>