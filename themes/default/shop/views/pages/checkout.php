<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $is_cod_active = $this->site->getActivePaymentMethod('COD','cod');
    if($is_cod_active){
        $cod_data = $this->site->getActivePaymentMethodData('COD','cod');
    }
?>

<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="panel panel-default margin-top-lg">
                            <div class="panel-heading text-bold">
                                <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('checkout'); ?>
                                <a href="<?= site_url('cart'); ?>" class="pull-right">
                                    <i class="fa fa-share"></i>
                                    <?= lang('back_to_cart'); ?>
                                </a>
                            </div>
                            <div class="panel-body">

                                <div>

                                    <!-- <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab"><?= lang('returning_user'); ?></a></li>
                                        <li role="presentation"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab"><?= lang('guest_checkout'); ?></a></li>
                                    </ul>

                                    <div class="tab-content padding-lg">
                                        <div role="tabpanel" class="tab-pane fade in active" id="user"> -->
                                            <?php
                                            if ($this->loggedIn) {
                                                if (!empty($addresses)) {
                                                    echo shop_form_open('order', 'class="validate"');
                                                    echo '<div class="row">';
                                                    echo '<div class="col-sm-12 text-bold">'.lang('select_address').'</div>';
                                                    $r = 1;
                                                    foreach ($addresses as $address) {
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="checkbox bg">
                                                                <label>
                                                                    <input type="radio" name="address" value="<?= $address->id; ?>" <?= $r == 1 ? 'checked' : ''; ?>>
                                                                    <span>
                                                                        <?= $address->line1; ?><br>
                                                                        <?= $address->line2; ?><br>
                                                                        <?= $address->city; ?> <?= $address->state; ?><br>
                                                                        <?= $address->postal_code; ?> <?= $address->country; ?><br>
                                                                        <?= lang('phone').': '.$address->phone; ?>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $r++;
                                                    }
                                                    echo '</div>';
                                                }
                                                if (count($addresses) < 6 && !$this->Staff) {
                                                    echo '<div class="row margin-bottom-lg">';
                                                    echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">'.lang('add_new_address').'</a></div>';
                                                    echo '</div>';
                                                }
                                                ?>
                                                <div class="form-group">
                                                    <?= lang('comment_any', 'comment'); ?>
                                                    <?= form_textarea('comment', set_value('comment'), 'class="form-control tip" id="comment" style="height:100px;"'); ?>
                                                </div>

                                                <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                                                    <div class="checkbox form-group">
                                                        <label>
                                                            <input name="redeemd_point" class="show" type="checkbox" value="1">Redeem Point(s)
                                                            [<?= ' You have '.$customer->award_points.' Award points ' ; ?>]
                                                        </label><br/>
                                                        <label><i class="fa fa-check"></i>If you want to use these reward points. Check the above checkbox.</label>
                                                    </div>
                                                <?php } ?>

                                                <?php if($is_cod_active){ ?>
                                                    <div class="checkbox form-group">
                                                        <label><input name="payment_by" class="show" type="checkbox" value="cod">Cash On Delivery</label><br/>
                                                        <?php if($this->sma->formatMoney($this->cart->total()) >  $this->sma->formatMoney($cod_data[0]->fixed_charges)){ ?>
                                                            <label><i class="fa fa-check"></i> Your order qualifies for FREE Shipping! </label>
                                                        <?php }else{ ?>
                                                            <label><i class="fa fa-info-circle"></i> Add <?= $this->sma->formatMoney($cod_data[0]->fixed_charges) - $this->sma->formatMoney($this->cart->total()) ?> AED of eligible item(s) to your order to qualify for FREE Shipping. <a target="_blank" href="<?=base_url('page/shipping-policy')?>" >Details</a> </label>
                                                        <?php } ?>

                                                    </div>
                                                <?php } ?>

                                                <?php
                                                if (!empty($addresses) && !$this->Staff) {
                                                    echo form_submit('add_order', lang('submit_order'), 'class="btn btn-theme"');
                                                } elseif ($this->Staff) {
                                                    echo '<div class="alert alert-warning margin-bottom-no">'.lang('staff_not_allowed').'</div>';
                                                } else {
                                                    echo '<div class="alert alert-warning margin-bottom-no">'.lang('please_add_address_first').'</div>';
                                                }
                                                echo form_close();
                                            } else {
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="well margin-bottom-no">
                                                            <?php  include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.'login_form.php';  ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h4 class="title"><span><?= lang('register_new_account'); ?></span></h4>
                                                        <p>
                                                            <?= lang('register_account_info'); ?>
                                                        </p>
                                                        <a href="<?= site_url('login#register'); ?>" class="btn btn-theme"><?= lang('register'); ?></a>
                                                        <!-- <a href="#" class="btn btn-default pull-right guest-checkout"><?= lang('guest_checkout'); ?></a> -->
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                            ?>
                                        <!-- </div>
                                        <div role="tabpanel" class="tab-pane fade" id="guest">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= lang('name', 'name'); ?>
                                                    <?= form_input('name', set_value('name'), 'class="form-control tip" id="name"  required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= lang('email', 'email'); ?>
                                                    <?= form_input('email', set_value('email'), 'class="form-control tip" id="email"  required="required"'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= lang('name', 'name'); ?>
                                                    <?= form_input('name', set_value('name'), 'class="form-control tip" id="name"  required="required"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div id="sticky-con" class="margin-top-lg">
                            <div class="panel panel-default">
                                <div class="panel-heading text-bold">
                                    <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('totals'); ?>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    $total = $this->sma->convertMoney($this->cart->total(), FALSE, FALSE);
                                    $shipping = $this->sma->convertMoney($this->cart->shipping(), FALSE, FALSE);
                                    $order_tax = $this->sma->convertMoney($this->cart->order_tax(), FALSE, FALSE);
                                    ?>
                                    <table class="table table-striped table-borderless cart-totals margin-bottom-no">
                                        <tr class="hide">
                                            <td><?= lang('total_w_o_tax'); ?></td>
                                            <td class="text-right"><?= $this->sma->convertMoney($this->cart->total()-$this->cart->total_item_tax()); ?></td>
                                        </tr>
                                        <tr class="hide">
                                            <td><?= lang('product_tax'); ?></td>
                                            <td class="text-right"><?= $this->sma->convertMoney($this->cart->total_item_tax()); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang('total'); ?></td>
                                            <td class="text-right"><?= $this->sma->formatMoney($total, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <?php if ($Settings->tax2 !== false) {
                                            echo '<tr><td>'.lang('order_tax').'</td><td class="text-right">'.$this->sma->formatMoney($order_tax, $selected_currency->symbol).'</td></tr>';
                                        } ?>
                                        <tr class="hide">
                                            <td><?= lang('shipping'); ?> *</td>
                                            <td class="text-right"><?= $this->sma->formatMoney($shipping, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <tr><td colspan="2"></td></tr>
                                        <tr class="active text-bold">
                                            <td><?= lang('grand_total'); ?></td>
                                            <td class="text-right"><?= $this->sma->formatMoney(($this->sma->formatDecimal($total)+$this->sma->formatDecimal($order_tax)+$this->sma->formatDecimal($shipping)), $selected_currency->symbol); ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>
            </div>
        </div>
    </div>
</section>
